import React, { Component } from "react";

class Footer extends Component {


  constructor(props) {
   super(props);

   this.state = {
     data: null,
   };
 }

render() {
  return (
    <div className="footer">
      <div className="footer-inner">
        <div className="footer-contact">
          <div className="footer-container"> 
            <i className="fas fa-frog" id="frog-logo"></i>
            <p className="footer-title">Hungry Frogg</p>
          </div> 
          <p>24 The Street</p>
          <p>NR1 2AB</p>
          <p>Norwich</p>
          <p>United Kingdom</p>
          <p>Tel: 01603 654321</p>
          <p>E-Mail: hungry@frog.co.uk</p> 
        </div>
        <div className="footer-social">
        <p>Follow us on Scoial Media:</p>
        <div className="social">
          <i className="fab fa-google-plus-square"></i>
          <i className="fab fa-facebook-square"></i>
          <i className="fab fa-twitter-square"></i>
          <i className="fab fa-instagram"></i>
        </div>
        </div>
      </div> 
    </div>
    );
  }
}
export default Footer;