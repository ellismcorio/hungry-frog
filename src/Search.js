import React, { Component } from "react";
import axios from 'axios';
 
class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {};

    this.handleChangeBox = this.handleChangeBox.bind(this); 
    this.handleChange = this.handleChange.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.filterSearch = this.filterSearch.bind(this);
  }

   handleChange(event) {
    event.preventDefault();
    this.state[event.target.name] = event.target.checked || event.target.value;
    this.setState(this.state );
  }

   handleChangeBox(event) {
    this.state[event.target.name] = event.target.checked || event.target.value;
    this.setState(this.state );
  }

  clearFilter(event) {
    this.setState({
      vegetarian: false,
      lowfat: false,
      alcoholfree: false,
      vegan: false,
      highprotein: false,
      balanced: false,
      lowcarb: false,
      lowsugar: false,
      tree: false,
      nut: false,
      value: '',
      calmin: '',
      calmax: '',
      ingredients: '',
    })
    var clist = document.getElementsByTagName("input");
    for (var i = 0; i < clist.length; ++i) { clist[i].checked = false};
    var tlist = document.getElementsByTagName("input");
    for (i = 0; i < tlist.length; ++i) { tlist[i].value = ''};
  };
  


  filterSearch(event) {
    document.getElementById("no-result").style.display = "none"
    let query = `https://api.edamam.com/search?q=${this.state.value}&ingr=${!this.state.calmin ? 5 :this.state.ingredients}&app_id=8d6a990b&app_key=35a0422f843e72eed81327f8e672a727&from=0&to=20&calories=${!this.state.calmin ? 20 : this.state.calmin}-${!this.state.calmax ? 2000 : this.state.calmax}&`;
    let list = Array.from(document.getElementsByClassName("diet-checkbox"))
    .filter(diet => diet.children[0].checked)
    .map(x => {
      let diet = "diet=";
      let health = "health=";
      let amp = "&";
      if (x.textContent === "balanced" || x.textContent === "low-fat" || x.textContent === "high-protein" || x.textContent === "low-carb" || x.textContent === "low-sugar") {
      return diet.concat(x.textContent).concat(amp)
      }
      else
      {
      return health.concat(x.textContent).concat(amp)
      }    
      });
      query = query.concat(list.join(''));
      axios.get(query)
      .then(response => {
        if (response.data.hits.length === 0) {
          document.getElementById("no-result").style.display = "flex"
        }
        else {
          this.setState({
        data: response.data.hits
      })
      document.getElementById("search-results").style.display="flex"
      }
    })
  };

  render() {

    return (
      <div className="search-main-content">
      <div className="logo-container">
        <i className="fas fa-frog" id="frog-logo"></i>
        <h1 className="app-title">Hungry Frogg</h1>
      </div>
      <div className="search-inner-content">
          <div className="search-form">
          <h1 className="app-title">Great Recipes</h1>
          <h1 className="app-title">Straight to You.</h1>
            <div className="search">
              <form onSubmit={this.handleChange}>
                <label>                
                  <input className="search-input" type="text" name="value" id="search-input" placeholder="search recipes here..." onChange={this.handleChange} />
                </label>
              </form>
              <div className="refine-search-section">
                <p>Refine search by:</p>
                <div className="cal-refine">
                  <p>calories:</p>
                  <input type="text" name="cal-min" className="cal-count" placeholder="20" onChange={this.handleChange} />
                  <input type="text" name="cal-max" className="cal-count" placeholder="2000" onChange={this.handleChange}/>
                </div>
                <div className="ing-refine">
                  <p>ingredients:</p>
                  <input type="text" name="ing-count" className="ing-count" placeholder="5" onChange={this.handleChange}/>                
                </div>
              </div>
            </div>
            <div className="diet-refine">
              <p className="diet-title">Diet:</p>
              <div className="diet-types">
                <label htmlFor="vege" className="diet-checkbox">
                vegetarian
                  <input type="checkbox" id="vege" className="diet-input" name="vegetarian" onClick={this.handleChangeBox}/>
                </label>
                <label htmlFor="fat" className="diet-checkbox">
                low-fat
                  <input type="checkbox" id="fat" className="diet-input" name="lowfat" onClick={this.handleChangeBox}/>
                </label>
                <label htmlFor="alco" className="diet-checkbox">
                alcohol-free
                  <input type="checkbox" id="alco" className="diet-input" name="alcoholfree" onClick={this.handleChangeBox}/>
                </label>
                <label htmlFor="vegan" className="diet-checkbox">
                vegan
                  <input type="checkbox" id="vegan" className="diet-input" name="vegan" onClick={this.handleChangeBox}/>
                </label>
                <label htmlFor="protein" className="diet-checkbox">
                high-protein
                  <input type="checkbox" id="protein" className="diet-input" name="highprotein"onClick={this.handleChangeBox}/>
                </label>
                <label htmlFor="balance" className="diet-checkbox">
                balanced
                  <input type="checkbox" id="balance" className="diet-input" name="balanced" onClick={this.handleChangeBox}/>
                </label>
                <label htmlFor="low" className="diet-checkbox">
                low-carb
                  <input type="checkbox" id="low" className="diet-input" name="lowcarb" onClick={this.handleChangeBox}/>
                </label>
                <label htmlFor="sugar" className="diet-checkbox">
                low-sugar
                  <input type="checkbox" id="sugar" className="diet-input" name="lowsugar" onClick={this.handleChangeBox}/>
                </label>
              </div>
            </div>
            <div className="allergy-refine">
              <p className="allergy-title">Allergy:</p>
              <div className="diet-types">
                <label htmlFor="treenuts" className="diet-checkbox">
                Tree-nuts
                  <input type="checkbox" id="treenuts" onClick={this.handleChangeTree}/>
                </label>
                <label htmlFor="peanuts" className="diet-checkbox">
                Peanuts
                  <input type="checkbox" id="peanuts" onClick={this.handleChangeNut}/>
                </label>
              </div>
            </div>
            <div className="clear-search">
              <button className="clear-filter-checkbox" onClick={this.clearFilter}>Clear Filter</button>
              <button className="refined-search" onClick={this.filterSearch}>Search</button>
            </div>
          </div>
          <div id="no-result" className="no-result">
            <i className="far fa-frown"></i>
            <p>Sorry, we found no recipes matching your search...</p>
          </div>    
          <div className="search-results" id="search-results">
            <ul className="search-list">
            {!this.state.data ? null : this.state.data.map((recipe, index) => (
              <li className="tile" key={index}>
                <img className="tile-image"src={recipe.recipe.image}/>
                <p className="tile-title">{recipe.recipe.label}</p>
                <div className="tile-nutrition">
                  <div className="tile-calories">
                    <p>{recipe.recipe.calories.toFixed(0)}</p>
                    <p>calories</p>
                  </div>
                  <div className="tile-ingredients">
                    <p>{recipe.recipe.ingredients.length}</p>
                    <p>ingredients</p>
                  </div>
                </div>
                <a className="tile-link"href={recipe.recipe.url}>{recipe.recipe.source}</a>
              </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
 
export default Search;