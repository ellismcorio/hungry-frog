import React, { Component } from "react";
import Search from './Search';
import Balanced from './Balanced'; 
import Lowfat from './Lowfat'; 
import Vegetarian from './Vegetarian';
import Footer from './Footer';

class Home extends Component {
  render() {
    return (
      <div className="main-content">
        <div className="image-container">
          <div className="text-content">
          <Search />   
          </div>
        </div>
        <div className="tiles-container">
          <ul className="tiles">
          <div className="balance-heading">
            <h2 className="category-title">Balanced Meals</h2>
          </div>           
            <Balanced />
            <div className="lowfat-heading">
              <h2 className="category-title">Lowfat Meals</h2>
            </div>
            <Lowfat />
            <div className="vegetarian-heading">
              <h2 className="category-title">Vegetarian Meals</h2>
            </div>
            <Vegetarian />            
          </ul>
        </div>
        <Footer />
      </div>
    );
  }
}
 
export default Home;