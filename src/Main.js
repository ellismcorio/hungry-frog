import React, { Component } from "react";
import {
  Route,
  NavLink,
  BrowserRouter,
} from "react-router-dom";
import Home from "./Home";

import Search from "./Search";
 
class Main extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="wrapper">
          <ul className="header">
            <li><NavLink exact to="/">Home</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={Home}/>    
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default Main;