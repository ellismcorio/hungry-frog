import React, { Component } from "react";
import axios from 'axios';


class Lowfat extends Component {


  constructor(props) {
   super(props);

   this.state = {
     data: null,
   };
 }

componentDidMount() {
  axios.get("https://api.edamam.com/search?q=recipe&app_id=8d6a990b&app_key=35a0422f843e72eed81327f8e672a727&from=0&to=20&calories=591-722&diet=low-fat")
  .then(response => this.setState({
    data: response.data.hits,
  }))
 }

render() {
 if(!this.state.data) return <div><i id="frog" className="fas fa-frog frog2"></i></div>
console.log(this.state.data);

  return (
    <div>
    <li className="low-fat">
      <ul className="tiles-row">
       {this.state.data.map((recipe, index) => (
        <li className="tile" key={index}>
          <img className="tile-image"src={recipe.recipe.image} alt=""/>
          <p className="tile-title">{recipe.recipe.label}</p>
          <div className="tile-nutrition">
            <div className="tile-calories">
              <p>{recipe.recipe.calories.toFixed(0)}</p>
              <p>calories</p>
            </div>
            <div className="tile-ingredients">
              <p>{recipe.recipe.ingredients.length}</p>
              <p>ingredients</p>
            </div>
          </div>
          <a className="ttile-link" id="outside" href={recipe.recipe.url}>{recipe.recipe.source}</a>
        </li>
         ))}
      </ul>
    </li>
    </div>

    );
  }
}
export default Lowfat;





